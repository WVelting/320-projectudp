const { start } = require("repl");

exports.PacketBuilder = 
{

    join(responseID, numOfUsers)
    {
        const packet = Buffer.alloc(6);

        packet.write("JOIN", 0);
        packet.writeUInt8(responseID, 4);
        packet.writeUInt8(numOfUsers, 5);
        return packet;

    },
    chat(lengthOfUsername, username, lengthOfMessage, message)
    {
        const packet = Buffer.alloc(7 + lengthOfUsername + lengthOfMessage);

        packet.write("CHAT", 0);
        packet.writeUInt8(lengthOfUsername, 4);
        packet.writeUInt8(lengthOfMessage, 5);
        packet.write(username, 6);
        packet.write(message, 7 + lengthOfUsername);
        return packet;

    },
    
    // start(game)
    // {
    //     const packet = Buffer.alloc(5);
    //     packet.write("STRT", 0);
    //     packet.writeUInt8(game.numOfClients, 4);
    //     return packet;

    // }
    
}