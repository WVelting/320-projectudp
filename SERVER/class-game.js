

exports.Game = class Game {

    static Singleton;

    constructor(server) {

        Game.Singleton = this;
        this.frame = 0;
        this.time = 0;
        this.dt = 16 / 1000;

        this.timeUntilNextStatePacket = 0;
        this.timeToUpdate = .1;

        this.objs = [];
		this.p1t1;
		this.p2t1;
		this.p1t2;
		this.p2t2;

        this.server = server;
        this.update();
    }

    spawnObject(obj) {
        this.objs.push(obj);

        let packet = Buffer.alloc(5);
        packet.write("REPL", 0);
        packet.writeUInt8(1, 4);

        const classID = Buffer.from(obj.classID);
        const data = obj.serialize();

        packet = Buffer.concat([packet, classID, data]);

        this.server.sendPacketToAll(packet);

    }

    removeObject(obj){
        const index = this.objs.indexOf(obj);
        if(index < 0) return; //obj doesn't exist

        const netID = this.objs[index].networkID; //get the id of the object

        this.objs.splice(index, 1); //remove object from array

        const packet = Buffer.alloc(6);
        packet.write("REPL", 0);
        packet.writeUInt8(3, 4); // 3 == delete
        packet.writeUInt8(netID, 5);

        this.server.sendPacketToAll(packet);

    }

    

    update() {
        this.time += this.dt;
        this.frame++;

        this.server.update(this);

        const player = this.server.getPlayer(0); //return the nth client;

        for (var i in this.objs)
        {
            this.objs[i].update(this);
        }

        if (player) {
            //do something
        }
        

        if (this.timeUntilNextStatePacket < this.timeToUpdate) {
            this.timeUntilNextStatePacket += this.dt;

        }
        else {
            this.timeUntilNextStatePacket -= this.timeToUpdate; //send a packet ever 100 ms
            this.sendWorldState();
        }
        setTimeout(() => this.update(), 16);
    }

    sendWorldState() {
        const packet = this.makeREPL(true);
        this.server.sendPacketToAll(packet);
    }

    makeREPL(isUpdate = true) {
        isUpdate = !!isUpdate;

        let packet = Buffer.alloc(5);
        packet.write("REPL", 0);
        packet.writeUInt8(isUpdate ? 2 : 1, 4);

        this.objs.forEach( o => {
            const classID = Buffer.from(o.classID);
            const data = o.serialize();

            packet = Buffer.concat([packet, classID, data]);
        });

        return packet;
    }
}