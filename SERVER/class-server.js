const { Client } = require("./class-client");
const { Game } = require("./class-game");


exports.Server = class Server {

	constructor() {
		this.port = 8080;
		this.serverName = "Will's Cool Server";
		this.clients = [];
		this.server = this;
		this.numOfUsers = 0;

		this.timeUntilNextBroadcast = 5;
		this.buffer = Buffer.alloc(0);


		this.sock = require("dgram").createSocket("udp4");

		//setup listeners
		this.sock.on("error", (e) => this.onError(e));
		this.sock.on("listening", () => this.onStartListen());
		this.sock.on("message", (msg, rinfo) => this.onPacket(msg, rinfo));


		//create game
		this.game = new Game(this);

		this.sock.bind(this.port);
	}

	onError(e) {
		console.log("ERROR: " + e);

	}
	onStartListen() {
		console.log("Server is listening on port " + this.port);

	}


	
	onPacket(msg, rinfo) {
		if (msg.length < 4) return;

		const packetID = msg.slice(0, 4).toString();

		const c = this.lookupClient(rinfo);

		if (c) {
			c.onPacket(msg, this.game);


		}
		
		else {
            if (packetID == "JOIN") {
                console.log("Someone tried to join from " + rinfo.address);
                this.makeClient(rinfo, this);
				
				const c = this.lookupClient(rinfo);
				c.onPacket(msg, this.game);
				
				
            }

        }
		



		//lookup and create clients
	}

	lookupClient(rinfo) {
		const key = this.getKeyFromRinfo(rinfo);
		return this.clients[key];
	}

	makeClient(rinfo, server) {

		const key = this.getKeyFromRinfo(rinfo);
		const client = new Client(rinfo, server);

		this.clients[key] = client;

		

		

		/*
			{
				"127.0.0.1:8080" : {rinfo: {}, input: {} etc etc},
			}

		*/

		//Pawn Stuff
		//--------------
		const packet = this.game.makeREPL(false);
		

		this.sendPacketToClient(packet, client);

		client.spawnPawn(this.game);
		this.showClientsList();

		const packet2 = Buffer.alloc(5);
		packet2.write("PAWN", 0);
		packet2.writeUInt8(client.pawn.networkID, 4);
		this.sendPacketToClient(packet2, client);

		//return client;


	}

	generateResponseID(desiredUsername, client) {
		if (desiredUsername.length < 3) return 6;
		if (desiredUsername.length > 12) return 7;

		const regex1 = /^[a-zA-Z0-9\[\]]+$/; //literal regex in javascript
		if (!regex1.test(desiredUsername)) return 8; //invalid characters

		let isUsernameTaken = false;
		this.clients.forEach(c => {
			if (c == client) return;
			if (c.username == desiredUsername) isUsernameTaken = true;
		});
		if (isUsernameTaken) return 9; //username taken

		const regex2 = /(fuck|fvck|shit|piss|damn)/i;
		if (regex2.test(desiredUsername)) return 10; //bad words



		if (!this.game.p1t1) {
			this.game.p1t1 = client;
			console.log("You are Payer One of Team One");
			this.numOfUsers++;

			return 1; //you are now player one on team one
		}

		if (!this.game.p2t1) {
			this.game.p2t1 = client;
			console.log("You are Player Two of Team One");
			this.numOfUsers++;
			return 2; //you are now Player Two of Team One
		}

		if (!this.game.p1t2) {
			this.game.p1t2 = client;
			console.log("You are Player One of Team Two");
			this.numOfUsers++;
			return 3; //you are now Player One of Team Two
		}

		if (!this.game.p2t2) {
			this.game.p2t2 = client;
			console.log("You are Player Two of Team Two");
			this.numOfUsers++;
			return 2; //you are now Player Two of Team Two
		}

		if (this.game.p1t1 == client) return 1; 
		if (this.game.p2t1 == client) return 2;
		if (this.game.p1t2 == client) return 3;
		if (this.game.p2t2 == client) return 4; 
		return 5; //you are a spectator

	}


	getPlayer(num = 0) {
		num = parseInt(num);
		let i = 0;
		for (var key in this.clients) {
			if (num == i) return this.clients[key];

			i++;
		}
	}

	showClientsList() {
		console.log(" ======== " + Object.keys(this.clients).length + " clients connected ========");
		for (var key in this.clients) {
			console.log(key);
		}
	}

	sendPacketToAll(packet) {
		for (var key in this.clients) {
			this.sendPacketToClient(packet, this.clients[key]);
		}
	}

	sendPacketToClient(packet, client) {
		console.log(client.rinfo);
		this.sock.send(packet, 0, packet.length, 8081, client.rinfo.address, () => { });
	}

	disconnectClient(client) {
		if (client.pawn) {
			//remove it
		}

		const key = this.getKeyFromRinfo(client.rinfo);
		delete this.clients[key];

		//TODO show clients list
		this.showClientsList();
		this.numOfUsers--;
	}

	getKeyFromRinfo(rinfo) {
		return rinfo.address + ":" + rinfo.port;
	}

	broadcastPacket(packet) {
		const clientListenPort = 8081;

		this.sock.send(packet, 0, packet.length, clientListenPort, undefined);

	}

	broadcastServerHost() {
		const nameLength = this.serverName.length;
		const packet = Buffer.alloc(7 + nameLength);

		packet.write("HOST", 0);
		packet.writeUInt16BE(this.port, 4);
		packet.writeUInt8(nameLength, 6);
		packet.write(this.serverName, 7);

		this.broadcastPacket(packet);
	}

	update(game) {
		//check clients for disconnects, etc
		for (let key in this.clients) {
			this.clients[key].update(game);
		}

		this.timeUntilNextBroadcast -= game.dt;
		if (this.timeUntilNextBroadcast <= 0) {
			this.timeUntilNextBroadcast = 1.5;

			this.broadcastServerHost();
		}

	}
}