const Game = require("./class-game.js").Game;
const Pawn = require("./class-pawn.js").Pawn;
const {PacketBuilder} = require("./PacketBuilder");

exports.Client = class Client {

    static TIMEOUT = 8;
    constructor(rinfo, server){

        this.rinfo = rinfo;
		this.server = server;
		this.username;
		

        this.input = {
            axisH: 0,
            axisV: 0
        };

        this.pawn = null;
		

        this.timeOfLastPacket = Game.Singleton.time; //measured in seconds
    }

    update(){
        const game = Game.Singleton;

        if(game.time > this.timeOfLastPacket + Client.TIMEOUT) {
            //disconnect
            game.server.disconnectClient(this);
        }
        

        
    }

	
    spawnPawn(game){
        if(this.pawn) return;

        this.pawn = new Pawn();
        //TODO spawns pawn
        game.spawnObject(this.pawn);
    }

    onPacket(packet, game) {
        if(packet.length < 4) return;

        const packetID = packet.slice(0,4).toString();


        this.timeOfLastPacket = game.time;
		console.log(packetID);

        switch(packetID){
            case "INPT":
                if(packet.length < 5) return;

                this.input.axisH = packet.readInt8(4);

                //TODO: add vertical axis

                //TODO: send input to pawn object

                if(this.pawn) this.pawn.input = this.input;
            break;

			case "JOIN": 
                if(packet.length <5) return;

                const lengthOfUsername = packet.readUInt8(4); //the 5th byte
                if(packet.length<5+lengthOfUsername) return;

                const desiredUsername = packet.slice(5, 5 + lengthOfUsername).toString();

                console.log("Client wants name to be " + desiredUsername);
                //check username...
                let responseType = this.server.generateResponseID(desiredUsername, this);
                this.username = desiredUsername;
                
                this.lengthOfUsername = lengthOfUsername;
                console.log(responseType);
                

                //consume the data
                packet = packet.slice(5 + lengthOfUsername);

                //respond
                let numOfUsers = this.server.numOfUsers;
                console.log("This many users:" + this.server.numOfUsers);
                const packet2 = PacketBuilder.join(responseType, numOfUsers);

                this.server.sendPacketToClient(packet2, this);

                // this.server.game.sendNumberOfClients(game);
                

                // const packet2 = PacketBuilder.update(this.server.game);
                // this.sendPacket(packet2);

                

                

                break;

				case "CHAT": 
                
                console.log("Chat received");
                if(packet.length<5) return;

                const lengthOfMessage = packet.readUInt8(4);
                if(packet.length<5+lengthOfMessage) return;
                
                
                
                
                const message = packet.slice(5, 5 + lengthOfMessage).toString();
                console.log(this.username + "wants to say: " +message);

                //consume the data
                packet = packet.slice(5 + lengthOfMessage);
                

                //respond
                const packet3 = PacketBuilder.chat(this.lengthOfUsername, this.username, lengthOfMessage, message);
                
                
                this.server.sendPacketToAll(packet3);
                console.log("Chat sent");


                break;
			

                

				
            default:
                console.log("ERROR: Packet type not recognized");
            break;
        }
    }
}