const { NetworkObject } = require("./class-networkobject");


exports.Pawn = class Pawn extends NetworkObject{

    constructor () {
        super();

        this.classID = "PAWN";

        this.input = {}
        this.velocity = {x:0, y:0, z:0};

    }

    accelerate(vel, acc, dt) {
        if(acc) {
            vel += acc * dt;
        } else {
            //not pressing left or right
            //slow down

            if(vel>0) {
                //moving right
                acc = 1;
                vel += acc*dt;
                if (vel <0) vel = 0;
            }

            if(vel<0) {
                //moving left
                acc = -1;
                vel += acc*dt;
                if(vel > 0) vel=0;
            }
        }

        return vel ? vel: 0;
    }
    
    update(game) {
        let moveX = this.input.axisH | 0;
        

        this.velocity.x = moveX * 100;

        this.position.x += this.velocity * game.dt;
    }

    serialize() {
        let b = super.serialize();

        return b;
    }

    deserialize() {
        //TODO: Maybe???
    }
}