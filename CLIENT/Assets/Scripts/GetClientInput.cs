using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetClientInput : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        Buffer b = PacketBuilder.CurrentInput();
        
        if(b != null)
        {
            //send that to the client UDP
            ClientUDP.singleton.SendPacket(b);
           
        }
    }
}
