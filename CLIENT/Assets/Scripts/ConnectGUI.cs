using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectGUI : MonoBehaviour
{
    
    public ServerRowGUI prefab;

    public GameObject Panel;



    

    public List<ServerRowGUI> rows = new List<ServerRowGUI>();
    float timeUntilRefresh = 1;

    private void Update()
    {
        timeUntilRefresh -= Time.deltaTime;

        if (Panel.activeInHierarchy)
        {
        if(timeUntilRefresh <= 0)
        {
            timeUntilRefresh = 2;
            //update the server list
            UpdateServerList();
        }

        }

        if (!Panel.activeInHierarchy)
        {
            
            foreach(ServerRowGUI row in rows)
            {
                if(row != null) Destroy(row.gameObject);
            }
            rows.Clear();
        }


    }

    public void UpdateServerList()
    {
        foreach (ServerRowGUI row in rows)
        {
            if(row != null) Destroy(row.gameObject);
        }
        //delete old prefabs
        rows.Clear();

        //check the clientUDP singleton
        int i = 0;
        print("Updating Server List");

        foreach(RemoteServer server in ClientUDP.singleton.availableGameServers)
        {
            print("Hey, these are servers");
            //spawn a prefab
            ServerRowGUI row = Instantiate(prefab, transform);

            RectTransform rect = (RectTransform) row.transform;
            rect.localScale = Vector3.one;
            rect.sizeDelta = new Vector2(500, 50);
            rect.anchorMax = rect.anchorMin = new Vector2(.025f, .5f);
            rect.anchoredPosition = new Vector2(0, -i * 70);

            row.Init(server);
            rows.Add(row);
            i++;
        }
    }
}