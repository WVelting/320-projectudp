﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using UnityEngine.UI;
using TMPro;
using System;

public class ClientUDP : MonoBehaviour
{

    private static ClientUDP _singleton;

    public static ClientUDP singleton
    {
        get { return _singleton; }
        private set { _singleton = value; }
    }

    static UdpClient sockReceive = new UdpClient(8081);
    static UdpClient sockSending;

    

    public List<RemoteServer> availableGameServers = new List<RemoteServer>();

    public GameObject PanelStart;
    public GameObject PanelConnect;
    public GameObject PanelLobby;
    public GameObject PanelGameplay;

    public TextMeshProUGUI serverWelcomeMessage;
    public TextMeshProUGUI chatWindow;

    public TMP_InputField inputUsername;
    public TMP_InputField inputHost;
    public TMP_InputField inputPort;
    public TMP_InputField chatEntry;

    public Button buttonStart;
    public TextMeshProUGUI readyUp;
    public TextMeshProUGUI startGame;

    public byte numOfUsers;

    public Transform ball;

    public enum Panel
    {
        Start,
        Connect,
        Lobby,
        Gameplay

    }

    void Start()
    {
        

        if (singleton != null)
        {
            Destroy(gameObject);
        }
        else
        {
            singleton = this;



            print("Starting Up");
            // set up receive loop (async):
            ListenForPackets();


            // send a packet to the server (async):
            // SendPacket(Buffer.From("JOIN"));
            GoToPanel(Panel.Start);
        }

        inputPort.text = "8080";
        inputHost.text = "127.0.0.1";


    }
    /// <summary>
    /// This function listens for incoming UDP packets.
    /// </summary>
    async void ListenForPackets()
    {
        while (true)
        {
            UdpReceiveResult res;
            try
            {
                res = await sockReceive.ReceiveAsync();
                ProcessPacket(res);
            }
            catch
            {
                break;
            }
        }
    }

    public void InputFieldEdit(string s)
    {
        Buffer packet = PacketBuilder.Chat(s);
        SendPacket(packet);
        chatEntry.text = "";
    }
    /// <summary>
    /// This function processes a packet and decides what to do next.
    /// </summary>
    /// <param name="packet">The packet to process</param>
    private void ProcessPacket(UdpReceiveResult res)
    {
        Buffer packet = Buffer.From(res.Buffer);

        if (packet.Length < 4) return; // do nothing

        string id = packet.ReadString(0, 4);

        print(id);
        switch (id)
        {
            case "HOST":
                if (packet.Length < 7) return;

                ushort port = packet.ReadUInt16BE(4);
                int nameLength = packet.ReadUInt8(6);

                if (packet.Length < 7 + nameLength) return;

                string name = packet.ReadString(7, nameLength);

                //do something with this info
                AddToServerList(new RemoteServer(res.RemoteEndPoint, name));

                break;
            case "JOIN":
                if(packet.Length<6) return;

                byte joinResponse = packet.ReadByte(4);
                numOfUsers = packet.ReadByte(5);
                print(numOfUsers);

                if(joinResponse == 1 || joinResponse == 2 || joinResponse == 3 || joinResponse == 4 || joinResponse == 5)
                {
                    readyUp.text = "Ready!";
                    if(numOfUsers >=4)
                    {
                        readyUp.text = "All Players Ready!";
                        startGame.text = "Start the Game!";
                    }
                }
                else if (joinResponse == 11) //full
                {
                    print("Game is full");
                    GoToPanel(Panel.Connect);
                }
                else if (joinResponse == 6|| joinResponse == 7|| joinResponse == 8|| joinResponse == 9|| joinResponse == 10)
                {
                    print("Username was bad, code: " + joinResponse);
                    GoToPanel(Panel.Lobby);
                }

                packet.Consume(6);
                break;

                case "PAWN":
                if(packet.Length < 5) return;

                byte networkID = packet.ReadUInt8(4);
                NetworkObject obj = NetworkObject.GetObjectByNetworkID(networkID);

                if(obj)
                {
                    Pawn p = (Pawn) obj;
                    if(p != null) p.canPlayerControl = true;
                }
                break;

                case "REPL":
                ProcessPacketREPL(packet);
            
                break;
                
                case "CHAT":

                if (packet.Length < 6) return;

                int usernameLength = packet.ReadByte(4);
                int messageLength = packet.ReadByte(5);

                int fullPacketLength = 7 + usernameLength + messageLength;

                if (packet.Length < fullPacketLength) 
                {
                    print("blahhh");
                    return;
                }

                string username = packet.ReadString(6, usernameLength);
                string message = packet.ReadString(7 + usernameLength, messageLength);

                print (message);
                print(username);

                //TODO: switch to gameplay:
                GoToPanel(Panel.Gameplay);

                //TODO: update chat view

                packet.Consume(fullPacketLength);

                AddMessageToChatDisplay(username, message);

                break;

        }

    }
    private void ProcessPacketREPL(Buffer packet)
    {
        if(packet.Length < 5) return; //do nothing

        int replType = packet.ReadUInt8(4);
        int offset = 5;
        if(replType < 1 || replType > 3) return; //do nothing

        while (offset <= packet.Length)
        {
            int networkID = 0;

            switch (replType)
            {
                case 1: //create
                    if(packet.Length< offset + 5) return;

                    networkID = packet.ReadUInt8(offset + 4);
                    
                    string classID = packet.ReadString(offset, 4);

                    //TODO networkobject
                    if(NetworkObject.GetObjectByNetworkID(networkID) != null) return; //object exists

                    NetworkObject obj = ObjectRegistry.SpawnFrom(classID);
                    if(obj == null) return; //ERROR

                    offset += 4;
                    offset += obj.Deserialize(packet.Slice(offset));

                    NetworkObject.AddObject(obj);

                break;
                case 2: //update

                    if(packet.Length<offset + 5) return;

                    networkID = packet.ReadUInt8(offset + 4);

                    NetworkObject obj2 = NetworkObject.GetObjectByNetworkID(networkID);
                    if(obj2 == null) return;

                    offset += 4;
                    offset += obj2.Deserialize(packet.Slice(offset));
                break;
                case 3: //delete

                    if(packet.Length < offset +1) return;

                    networkID = packet.ReadUInt8(offset);

                    NetworkObject obj3 = NetworkObject.GetObjectByNetworkID(networkID);
                    if(obj3 == null)return;

                    NetworkObject.RemoveObject(networkID);

                    Destroy(obj3.gameObject);
                    offset++;
                break;
            }
        }
    }

    public void AddMessageToChatDisplay(string user, string mess)
    {
        chatWindow.text += user + ": " + mess + "\n";
    }

    public void OnButtonStart()
    {
        GoToPanel(Panel.Connect);

    }
    /// <summary>
    /// This function sends a packet (current to localhost:320)
    /// </summary>
    /// <param name="packet">The packet to send</param>
    public async void SendPacket(Buffer packet)
    {
        // TODO: remove literals from next line:
        if (sockSending == null) return;
        if (!sockSending.Client.Connected) return;
        await sockSending.SendAsync(packet.Bytes, packet.Bytes.Length);
    }
    /// <summary>
    /// When destroying, clean up objects:
    /// </summary>
    private void OnDestroy()
    {
        if (sockSending != null) sockSending.Close();
        if (sockReceive != null) sockReceive.Close();
    }

    public void ConnectToServer(string host, ushort port)
    {
        print($"attempt to connect to {host}:{port}");

        //TODO: dont do anything if we're already connected

        IPEndPoint ep = new IPEndPoint(IPAddress.Parse(host), port);
        sockSending = new UdpClient(ep.AddressFamily);
        sockSending.Connect(ep);
        
        
        GoToPanel(Panel.Lobby);

        //SendPacket(Buffer.From("JOIN"));
    }

    public void onClickConnect()
    {
        string host = inputHost.text;
        string sPort = inputPort.text;
        ushort port = Convert.ToUInt16(sPort);
        ConnectToServer(host, port);
    }

    public void onClickReady()
    {
        string user = inputUsername.text;
        //create packet
        Buffer packet = PacketBuilder.Join(user);
        //send packet
        print(user);
        SendPacket(packet);
        
    }

    public void onStartGame()
    {
        if(numOfUsers >= 4)
        {
            GoToPanel(Panel.Gameplay);
        }
    }

    private void AddToServerList(RemoteServer server)
    {
        print("Adding to Server List");
        if (!availableGameServers.Contains(server))
        {
            availableGameServers.Add(server);
            print("Server added to list");

        }

        serverWelcomeMessage.text = "Welcome to " + server.serverName;
    }

    public void GoToPanel(Panel panel)
    {
        if (panel == Panel.Start)
        {
            print("Going to Start Panel");
            PanelStart.SetActive(true);
            PanelConnect.SetActive(false);
            PanelLobby.SetActive(false);
            PanelGameplay.SetActive(false);
        }
        else if (panel == Panel.Connect)
        {
            PanelStart.SetActive(false);
            PanelConnect.SetActive(true);
            PanelLobby.SetActive(false);
            PanelGameplay.SetActive(false);
        }
        else if (panel == Panel.Lobby)
        {
            PanelStart.SetActive(false);
            PanelConnect.SetActive(false);
            PanelLobby.SetActive(true);
            PanelGameplay.SetActive(false);
        }
        else if (panel == Panel.Gameplay)
        {
            PanelStart.SetActive(false);
            PanelConnect.SetActive(false);
            PanelLobby.SetActive(false);
            PanelGameplay.SetActive(true);
        }
    }
}


