using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public static class PacketBuilder
{
    static int previousInputH = 0;
    //join
    public static Buffer Join(string username)
    {
        
        int packetLength = 5 + username.Length;
        Buffer packet = Buffer.Alloc(packetLength);

        packet.WriteString("JOIN");
        packet.WriteByte((byte)username.Length, 4);
        packet.WriteString(username, 5);
        return packet;
    }

    //chat
    public static Buffer Chat(string message)
    {
        
        int packetLength = 5 + message.Length;
        Buffer packet = Buffer.Alloc(packetLength);

        packet.WriteString("CHAT");
        packet.WriteByte((byte)message.Length,4);
        packet.WriteString(message,5);
        return packet;
    }

    //play

    public static Buffer CurrentInput()
    {
        int h = (int)Input.GetAxisRaw("Horizontal");

        if (h== previousInputH) return null;

        previousInputH = h;

        Buffer b = Buffer.Alloc(5);
        b.WriteString("INPT", 0);
        b.WriteInt8((sbyte)h, 4);

        return b;

    }
}
